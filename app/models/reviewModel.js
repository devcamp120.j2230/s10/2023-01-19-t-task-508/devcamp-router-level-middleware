//B1: Khai báo thư viện mongoose
const mongoose = require("mongoose");

//B2: Khai báo đối tượng Schema
const Schema = mongoose.Schema;

//B3: Tạo ra đối tượng schema tương ứng với từng collection
const reviewSchema = new Schema({
    _id:mongoose.Types.ObjectId,
    stars:{
        type:Number,
        default:0
    },
    note:{
        type:String,
        required:false
    }
});

//B4: export schemal ra modul để thao tác với mongodb (crud)
module.exports = mongoose.model("review", reviewSchema);