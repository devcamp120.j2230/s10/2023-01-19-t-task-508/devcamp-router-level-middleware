//B1: Khai báo thư viện mongoose
const mongoose = require("mongoose");

//B2: Khai báo đối tượng Schema
const Schema = mongoose.Schema;

//B3: Tạo ra đối tượng schema tương ứng với từng collection
const courseSchema = new Schema({
    _id:mongoose.Types.ObjectId,
    title:{
        type:String,
        required:true,
        unique:true
    },
    description:String,
    noStudent:{
        type:Number,
        default:0
    },
    reviews:[{
        type:mongoose.Types.ObjectId,
        ref:"review"
    }]
})

//B4: export schemal ra modul để thao tác với mongodb (crud)
module.exports = mongoose.model("course", courseSchema);