const reviewMiddleware = (request, response, next) => {
    console.log("review middleware - date:" + new Date() + " - method: " + request.method);
    next();
}

const getAllReviewMiddleware = (request, response, next) => {
    console.log("Get all Review!");
    next();
}

const getReviewMiddleware = (request, response, next) => {
    console.log("Get a Review!");
    next();
}

const createReviewMiddleware = (request, response, next) => {
    console.log("Create a Review!");
    next();
}

const updateReviewMiddleware = (request, response, next) => {
    console.log("Update a Review!");
    next();
}

const deleteReviewMiddleware = (request, response, next) => {
    console.log("Delete a Review!");
    next();
}

module.exports = { 
    reviewMiddleware,
    getAllReviewMiddleware,
    getReviewMiddleware,
    createReviewMiddleware,
    updateReviewMiddleware,
    deleteReviewMiddleware
}