const courseMiddleware = (request, response, next) => {
    console.log("course middleware - date:" + new Date() + " - method: " + request.method);
    next();
}


const getAllCourseMiddleware = (request, response, next) => {
    console.log("Get all course!");
    next();
}

const getCourseMiddleware = (request, response, next) => {
    console.log("Get a course!");
    next();
}

const createCourseMiddleware = (request, response, next) => {
    console.log("Create a course!");
    next();
}

const updateCourseMiddleware = (request, response, next) => {
    console.log("Update a course!");
    next();
}

const deleteCourseMiddleware = (request, response, next) => {
    console.log("Delete a course!");
    next();
}

module.exports = { 
    courseMiddleware, 
    getAllCourseMiddleware,
    getCourseMiddleware,
    createCourseMiddleware,
    updateCourseMiddleware,
    deleteCourseMiddleware
}