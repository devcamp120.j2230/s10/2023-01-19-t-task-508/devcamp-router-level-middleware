//Khai báo thư viện express
const express = require("express");

//Khai báo thư viện mongoose
const mongoose = require("mongoose")

//Khai báo các router
const { courseRouter } = require("./app/routes/courseRouter");
const { reviewRouter } = require("./app/routes/reviewRouter");

//khai báo model
const { reviewModel } = require("./app/models/reviewModel");
const { courseModel } = require("./app/models/courseModel");

//khởi tạo app
const app = express()

//khai báo cổng
const port = 8000;

//kết nối tới mongoose
mongoose.connect('mongodb://127.0.0.1:27017/CRUD_Course', function(error) {
    if (error) throw error;
    console.log('Successfully connected to MongoDB!');
})   

const methodMiddeware = (req, res, next) => {
    console.log(req.method);
    next();
};
//Task 506.10: application middeware
app.use('/get-method', (req, res, next) => {
    console.log(new Date());
    next();
}, methodMiddeware);

app.post('/post-method', (req, res, next) => {
    console.log("Only post method");
    next();
});

/*
app.use((req, res, next) => {
    console.log(req.method);
    next();
});*/

app.post('/post-method', (req, res) => {
    console.log('Post Method');
    res.json({
        message:"Post Method"
    })
});

app.get('/get-method', (req, res) => {
    console.log('Get Method');
    res.json({
        message:"Get Method"
    })
});

//khai báo api trả về chuỗi
app.get('/', (req, res) => {
    var date = new Date();
    //console.log(date);
    res.status(200).json({
        message:`Hôm nay là ngày ${date.getDate()} tháng ${date.getMonth()} năm ${date.getFullYear()}`
    })

})

//Task 506.20: router middeware
app.use('/', courseRouter);
app.use('/', reviewRouter);

//start ứng dụng express
app.listen(port, () => {
    console.log(`App runing on port ${port}`);
})
